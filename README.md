A docker-compose setup to reproduce issues during application redeployment on Tomcat server.

Perform the test with: `docker-compose rm -fs && docker-compose up --build`
(be patient, the very first startup will be slow,
because of the building and initializing of Oracle database)

### Steps overview

- An empty Tomcat server and Oracle XE database startup
- The 1st application gets deployed
- When 1st app is deployed, the 2nd version starts being deployed
- The 1st app gets undeployed (either manually 
    or with `undeployOldVersions="true"` in Tomcat's `conf/server.xml`, 
    see Tomcat's [Parallel deployment feature](https://tomcat.apache.org/tomcat-9.0-doc/config/context.html#Parallel_deployment))
- The http request involving `ResultSetMetaData` gets performed for the 2nd app

### The ResultSetMetaData problem

With ojdbc/ucp v12.2.0.1 during test request from the last step 
you will get the following error in Tomcat's stdout and http 500:

```
java.lang.IllegalStateException: Illegal access: this web application instance has been stopped already.
  Could not load [java.sql.ResultSetMetaData]
```

Every subsequent request will also fail with: `java.lang.NoClassDefFoundError: java/sql/ResultSetMetaData`

This problem can be solved by either upgrading Oracle jars or by moving ucp.jar from 
`$CATALINA_BASE/lib` to app's `.war` by setting `compile` scope in pom.xml. 
But moving ucp.jar to war breaks the `PoolDataSourceImpl.class#getAnnotations()` which leads to
other problems.

For Tomcat 7 The problem did not affect the observable behavior, but can be still caught in logs.

### The potential memory leak problem

On application's unload there are the following warnings from Tomcat:

```
21-Aug-2021 18:02:25.247 WARNING [Catalina-utility-2] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [app##1] appears to have started a thread named [Timer-0] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 java.lang.Object.wait(Object.java:502)
 java.util.TimerThread.mainLoop(Timer.java:526)
 java.util.TimerThread.run(Timer.java:505)

21-Aug-2021 18:02:25.249 WARNING [Catalina-utility-2] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [app##1] appears to have started a thread named [oracle.jdbc.driver.BlockSource.ThreadedCachingBlockSource.BlockReleaser] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 oracle.jdbc.driver.BlockSource$ThreadedCachingBlockSource$BlockReleaser.run(BlockSource.java:329)

21-Aug-2021 18:02:25.251 WARNING [Catalina-utility-2] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [app##1] appears to have started a thread named [InterruptTimer] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 java.lang.Object.wait(Object.java:502)
 java.util.TimerThread.mainLoop(Timer.java:526)
 java.util.TimerThread.run(Timer.java:505)
```

Not seem to be caused by UCP, you can use `oracle.jdbc.pool.OracleDataSource` from ojdbc.jar,
and still get the above warnings.

Also tried:
- using other pools with invocation of `close` method:
  - Simple DBCP's BasicDataSource
  - HikariCP
- using lifecycle methods of `UniversalConnectionPoolManager`
  - at `@PostConstruct` and `@PreDestroy`
  - at `ServletContextListener#contextDestroyed`
