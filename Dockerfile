FROM maven:3.8.2-jdk-8-slim as build
ARG BUILD_NUMBER
ARG ORCL_JARS_VERSION
WORKDIR /build
COPY . .
RUN --mount=type=cache,target=/root/.m2 mvn clean package \
    -Dbuild.number=${BUILD_NUMBER} -Dojdbc.version=${ORCL_JARS_VERSION} --batch-mode

FROM curlimages/curl:latest
USER root
WORKDIR /artifacts
COPY --from=build /build/target/*.war .
