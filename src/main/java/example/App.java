package example;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@RestController
@Configuration
@PropertySource("classpath:app.properties")
@EnableWebMvc
@ComponentScan(basePackageClasses = App.class)
public class App {
    @Bean
    JdbcTemplate jdbcTemplate(DataSource ds) {
        return new JdbcTemplate(ds);
    }

    @Bean
    @Profile("!hikari")
    DataSource dataSource(
            @Value("${jdbc.url}") String url,
            @Value("${jdbc.user}") String user,
            @Value("${jdbc.password}") String password
    ) throws SQLException {
        PoolDataSource ds = PoolDataSourceFactory.getPoolDataSource();
        ds.setURL(url);
        ds.setUser(user);
        ds.setPassword(password);
        ds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
        return ds;
    }

    @Bean
    @Profile("hikari")
    DataSource hikariDs(
            @Value("${jdbc.url}") String url,
            @Value("${jdbc.user}") String user,
            @Value("${jdbc.password}") String password
    ) {
        HikariConfig cfg = new HikariConfig();
        cfg.setJdbcUrl(url);
        cfg.setUsername(user);
        cfg.setPassword(password);
        cfg.setDriverClassName("oracle.jdbc.OracleDriver");
        return new HikariDataSource(cfg);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Value("${build.number}")
    String buildNumber;

    @PostConstruct
    public Collection<String> initPool() {
        List<String> resp = jdbcTemplate.query("select 'hello-db' from dual", (rs, rowNum) -> rs.getString(1));
        System.out.println("!!!!!!!!!!Connection pool initialized by a simple query for app v" + buildNumber);
        return resp;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/build-number")
    @ResponseBody
    public String buildNumber() {
        return buildNumber;
    }


    @RequestMapping(method = RequestMethod.GET, path = "/rs-metadata-test")
    @ResponseBody
    public String rsMetadataTest() {
        System.out.println("Invoking app of version:" + buildNumber);
        return jdbcTemplate.queryForObject("select 'it works' from dual", String.class);
    }
}
