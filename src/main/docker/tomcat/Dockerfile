FROM bellsoft/liberica-openjre-alpine:8
WORKDIR /opt/tomcat
ARG TOMCAT_VERSION
RUN if [ "${TOMCAT_VERSION}" = "9.0" ]; then \
      DL_URL="https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.52/bin/apache-tomcat-9.0.52.tar.gz" ; \
    elif [ "${TOMCAT_VERSION}" = "8.5" ]; then \
      DL_URL="https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.70/bin/apache-tomcat-8.5.70.tar.gz" ; \
    elif [ "${TOMCAT_VERSION}" = "8.0" ]; then \
      DL_URL="https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.53/bin/apache-tomcat-8.0.53.tar.gz" ; \
    elif [ "${TOMCAT_VERSION}" = "7.0" ]; then \
      DL_URL="https://archive.apache.org/dist/tomcat/tomcat-7/v7.0.109/bin/apache-tomcat-7.0.109.tar.gz" ; \
    else \
      echo "Unknown TOMCAT_VERSION! Available values are: 9.0, 8.5, 8.0 and 7.0 " && false;  \
    fi; \
wget -qO- $DL_URL | tar xvz --strip-components=1
EXPOSE 8080
RUN sed -i -E 's/(<Host.+$)/\1 undeployOldVersions="true"/' conf/server.xml && \
    grep 'undeployOldVersions' conf/server.xml || (echo 'Failed to configure tomcat to enable parallel deployment' && false)

ARG ORCL_JARS_TO_LIB=ojdbc8,ucp
ARG ORCL_JARS_VERSION=12.2.0.1
RUN echo ${ORCL_JARS_TO_LIB} | tr ',' '\n' | xargs -P 2 -I{} \
    wget -P lib https://repo1.maven.org/maven2/com/oracle/database/jdbc/{}/${ORCL_JARS_VERSION}/{}-${ORCL_JARS_VERSION}.jar

CMD ["bin/catalina.sh", "run"]
